import domain.Lawn;
import domain.MowerMovementResult;
import formatter.LawnFormatter;
import parser.LawnParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MowerApplication {

    public static void main(String[] args) throws URISyntaxException, IOException {
        Path path = Paths.get(MowerApplication.class.getClassLoader().getResource("inputFile.txt").toURI());
        run(path, new LawnParser(), new LawnFormatter());
    }

    private static void run(Path path, LawnParser parser, LawnFormatter formatter) throws IOException {
        List<String> inputString = Files.readAllLines(path);
        Lawn lawn = parser.parse(inputString);
        List<MowerMovementResult> mowerMovementResults = lawn.mow();
        String result = formatter.format(mowerMovementResults);
    }
}
