package domain;

import java.util.List;
import java.util.Objects;

public class Mower {

    private Position position;

    private Orientation orientation;

    private final List<Instruction> instructions;

    public Mower(Position position, Orientation orientation, List<Instruction> instructions) {
        this.position = position;
        this.orientation = orientation;
        this.instructions = instructions;
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public void goLeft() {
        switch (this.orientation) {
            case NORTH:
                this.orientation = Orientation.WEST;
                break;
            case EAST:
                this.orientation = Orientation.NORTH;
                break;
            case SOUTH:
                this.orientation = Orientation.EAST;
                break;
            case WEST:
                this.orientation = Orientation.SOUTH;
                break;
        }
    }

    public void goRight() {
        switch (this.orientation) {
            case NORTH:
                this.orientation = Orientation.EAST;
                break;
            case EAST:
                this.orientation = Orientation.SOUTH;
                break;
            case SOUTH:
                this.orientation = Orientation.WEST;
                break;
            case WEST:
                this.orientation = Orientation.NORTH;
                break;
        }
    }

    public void goForward() {
        this.position = Position.advance(this.position, this.orientation);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mower mower = (Mower) o;
        return Objects.equals(position, mower.position) && orientation == mower.orientation && Objects.equals(instructions, mower.instructions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, orientation, instructions);
    }
}
