package domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Lawn {

    private final Dimension dimension;

    private final List<Mower> mowers;

    public Lawn(Dimension dimension, List<Mower> mowers) {
        this.dimension = dimension;
        this.mowers = mowers;
    }

    public List<MowerMovementResult> mow() {
        List<MowerMovementResult> mowerMovementResults = new ArrayList<>();
        for (Mower currentMower : this.mowers) {
            List<Position> occupiedPositions = mowers.stream()
                    .map(Mower::getPosition)
                    .filter(position -> !currentMower.getPosition().equals(position))
                    .collect(Collectors.toList());
            MowerMovementResult mowerMovementResult = moveMower(currentMower, occupiedPositions);
            mowerMovementResults.add(mowerMovementResult);
        }
        return mowerMovementResults;
    }

    private MowerMovementResult moveMower(Mower mower, List<Position> occupiedPositions) {
        for (Instruction instruction : mower.getInstructions()) {
            switch (instruction) {
                case FORWARD :
                    Position futurePosition = Position.advance(mower.getPosition(), mower.getOrientation());
                    boolean positionIsFree = occupiedPositions.stream().noneMatch(position -> position.equals(futurePosition));
                    boolean positionIsInTheLawn = Dimension.isInside(futurePosition, dimension);
                    if (positionIsFree && positionIsInTheLawn) {
                        mower.goForward();
                    }
                    break;
                case LEFT :
                    mower.goLeft();
                    break;
                case RIGHT :
                    mower.goRight();
                    break;
                default : throw new IllegalStateException();
            }
        }

        return new MowerMovementResult(mower.getPosition(), mower.getOrientation());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lawn lawn = (Lawn) o;
        return Objects.equals(dimension, lawn.dimension) && Objects.equals(mowers, lawn.mowers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dimension, mowers);
    }
}
