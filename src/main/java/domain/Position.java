package domain;

import java.util.Objects;

public class Position {

    private final int x;

    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static Position advance(Position position, Orientation orientation) {
        switch (orientation) {
            case NORTH:
                return new Position(position.getX(), position.getY() + 1);
            case SOUTH:
                return new Position(position.getX(), position.getY() - 1);
            case EAST:
                return new Position(position.getX() + 1, position.getY());
            case WEST:
                return new Position(position.getX() -1, position.getY());
        }
        throw new IllegalStateException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
