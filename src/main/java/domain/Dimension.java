package domain;

import java.util.Objects;

public class Dimension {

    private final int length;

    private final int width;

    public Dimension(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public static boolean isInside(Position position, Dimension dimension) {
        return position.getX() >= 0 &&
                position.getY() >= 0 &&
                position.getX() < dimension.getLength() &&
                position.getY() < dimension.getWidth();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dimension dimension = (Dimension) o;
        return length == dimension.length && width == dimension.width;
    }

    @Override
    public int hashCode() {
        return Objects.hash(length, width);
    }
}
