package domain;

import java.util.Objects;

public class MowerMovementResult {

    private final Position position;

    private final Orientation orientation;

    public MowerMovementResult(Position position, Orientation orientation) {
        this.position = position;
        this.orientation = orientation;
    }

    public Position getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MowerMovementResult that = (MowerMovementResult) o;
        return Objects.equals(position, that.position) && orientation == that.orientation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, orientation);
    }
}
