package domain;

public enum Instruction {

    FORWARD,
    LEFT,
    RIGHT

}
