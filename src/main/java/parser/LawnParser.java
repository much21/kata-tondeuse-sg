package parser;

import domain.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LawnParser {

    private static final String SPACE = " ";

    public Lawn parse(List<String> stringList) {
        String firstLine = stringList.get(0);
        Dimension dimension = parseDimension(firstLine);
        List<Mower> mowers = new ArrayList<>();
        for (int i = 1 ; i < stringList.size() ; i = i + 2) {
            Mower mower = parseMower(i, stringList);
            mowers.add(mower);
        }
        return new Lawn(dimension, mowers);
    }

    private Dimension parseDimension(String firstLine) {
        String[] firstLineArray = firstLine.split(SPACE);
        return new Dimension(
                Integer.parseInt(firstLineArray[0]) + 1,
                Integer.parseInt(firstLineArray[1]) + 1);
    }

    private Mower parseMower(int i, List<String> stringList) {
        String firstMowerLine = stringList.get(i);
        String[] firstMowerLineArray = firstMowerLine.split(SPACE);
        Position position = new Position(
                Integer.parseInt(firstMowerLineArray[0]),
                Integer.parseInt(firstMowerLineArray[1]));
        Orientation orientation = parseOrientation(firstMowerLineArray[2]);
        String secondMowerLine = stringList.get(i +1);
        List<Instruction> instructions = parseInstruction(secondMowerLine);
        return new Mower(position, orientation, instructions);
    }

    private Orientation parseOrientation(String orientation) {
        switch (orientation) {
            case "N" : return Orientation.NORTH;
            case "E" : return Orientation.EAST;
            case "W" : return Orientation.WEST;
            case "S" : return Orientation.SOUTH;
        }
        throw new IllegalStateException();
    }

    private List<Instruction> parseInstruction(String instructionLetters) {
        List<Instruction> instructions = new ArrayList<>();
        for (String instruction : Arrays.asList(instructionLetters.split(""))) {
            switch (instruction) {
                case "A" :
                    instructions.add(Instruction.FORWARD);
                    break;
                case "G" :
                    instructions.add(Instruction.LEFT);
                    break;
                case "D" :
                    instructions.add(Instruction.RIGHT);
                    break;
                default:
                    throw new IllegalStateException();
            }
        }
        return instructions;
    }
}
