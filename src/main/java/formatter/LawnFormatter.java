package formatter;

import domain.MowerMovementResult;
import domain.Orientation;

import java.util.List;
import java.util.stream.Collectors;

public class LawnFormatter {

    private static final String SPACE = " ";

    public String format(List<MowerMovementResult> mowerMovementResults) {
        return mowerMovementResults.stream()
                .map(movementResult -> String.format(
                        "%d %d %s",
                        movementResult.getPosition().getX(),
                        movementResult.getPosition().getY(),
                        getOrientation(movementResult.getOrientation())
                ))
                .collect(Collectors.joining(SPACE));
    }

    private String getOrientation(Orientation orientation) {
        switch (orientation) {
            case NORTH:
                return "N";
            case EAST:
                return "E";
            case WEST:
                return "O";
            case SOUTH:
                return "S";
        }
        throw new IllegalStateException();
    }
}
