package parser;

import domain.Dimension;
import domain.Lawn;
import domain.Mower;
import domain.Position;
import org.junit.jupiter.api.Test;

import java.util.List;

import static domain.Instruction.*;
import static domain.Orientation.EAST;
import static domain.Orientation.NORTH;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LawnParserTest {

    @Test
    public void shouldParseStringList() {
        List<String> lines = List.of(
                "5 5",
                "1 2 N",
                "GAGAGAGAA",
                "3 3 E",
                "AADAADADDA"
        );

        LawnParser lawnParser = new LawnParser();
        Lawn lawnResult = lawnParser.parse(lines);
        Lawn expectedLawn = new Lawn(new Dimension(6,6), List.of(
                new Mower(new Position(1,2), NORTH, List.of(LEFT, FORWARD, LEFT, FORWARD, LEFT, FORWARD, LEFT, FORWARD, FORWARD)),
                new Mower(new Position(3,3), EAST, List.of(FORWARD, FORWARD, RIGHT, FORWARD, FORWARD, RIGHT, FORWARD, RIGHT, RIGHT, FORWARD))
        ));

        assertEquals(expectedLawn, lawnResult);
    }
}
