package formatter;

import domain.MowerMovementResult;
import domain.Position;
import org.junit.jupiter.api.Test;

import java.util.List;

import static domain.Orientation.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LawnFormatterTest {

    @Test
    public void shouldFormatStringList() {
        List<MowerMovementResult> mowerMovementResults = List.of(
                new MowerMovementResult(new Position(2,4), NORTH),
                new MowerMovementResult(new Position(3, 1), EAST),
                new MowerMovementResult(new Position(0,2), WEST)
        );

        LawnFormatter lawnFormatter = new LawnFormatter();
        String result = lawnFormatter.format(mowerMovementResults);
        String expected = "2 4 N 3 1 E 0 2 O";

        assertEquals(expected, result);
    }
}
