import domain.*;
import org.junit.jupiter.api.Test;

import java.util.List;

import static domain.Instruction.*;
import static domain.Orientation.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MowerKataTests {

    @Test
    public void shouldWork() {
        Dimension dimension = new Dimension(6,6);
        Mower mower = new Mower(new Position(1,2), NORTH, List.of(LEFT, FORWARD, LEFT, FORWARD, LEFT, FORWARD, LEFT, FORWARD, FORWARD));
        Mower mower2 = new Mower(new Position(3,3), EAST, List.of(FORWARD, FORWARD, RIGHT, FORWARD, FORWARD, RIGHT, FORWARD, RIGHT, RIGHT, FORWARD));
        Lawn lawn = new Lawn(dimension, List.of(mower, mower2));

        List<MowerMovementResult> mowerMovementResults = lawn.mow();
        List<MowerMovementResult> mowerMovementExpected = List.of(
                new MowerMovementResult(new Position(1, 3), NORTH),
                new MowerMovementResult(new Position(5, 1), EAST)
        );

        assertEquals(mowerMovementExpected, mowerMovementResults);
    }
}
