package domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MowerTest {

    @ParameterizedTest
    @MethodSource("getParametersForShouldGoLeft")
    public void shouldGoLeft(Orientation initialOrientation, Orientation expectedOrientation) {
        Mower mower = new Mower(
                new Position(1,1),
                initialOrientation,
                List.of(Instruction.LEFT)
        );
        mower.goLeft();

        assertEquals(expectedOrientation, mower.getOrientation());
    }

    @ParameterizedTest
    @MethodSource("getParametersForShouldGoRight")
    public void shouldGoRight(Orientation initialOrientation, Orientation expectedOrientation) {
        Mower mower = new Mower(
                new Position(1,1),
                initialOrientation,
                List.of(Instruction.RIGHT)
        );
        mower.goRight();

        assertEquals(expectedOrientation, mower.getOrientation());
    }

    private static Stream<Arguments> getParametersForShouldGoLeft() {
        return Stream.of(
                Arguments.of(Orientation.NORTH, Orientation.WEST),
                Arguments.of(Orientation.EAST, Orientation.NORTH),
                Arguments.of(Orientation.SOUTH, Orientation.EAST),
                Arguments.of(Orientation.WEST, Orientation.SOUTH)
        );
    }

    private static Stream<Arguments> getParametersForShouldGoRight() {
        return Stream.of(
                Arguments.of(Orientation.NORTH, Orientation.EAST),
                Arguments.of(Orientation.EAST, Orientation.SOUTH),
                Arguments.of(Orientation.SOUTH, Orientation.WEST),
                Arguments.of(Orientation.WEST, Orientation.NORTH)
        );
    }
}
