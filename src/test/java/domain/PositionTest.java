package domain;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PositionTest {

    @ParameterizedTest
    @MethodSource("getParametersForShouldAdvance")
    public void shouldAdvance(Orientation orientation, Position expectedPosition) {
        Position initialPosition = new Position(2,2);
        Position result = Position.advance(initialPosition, orientation);

        assertEquals(expectedPosition, result);
    }

    private static Stream<Arguments> getParametersForShouldAdvance() {
        return Stream.of(
                Arguments.of(Orientation.NORTH, new Position(2, 3)),
                Arguments.of(Orientation.EAST, new Position(3, 2)),
                Arguments.of(Orientation.WEST, new Position(1, 2)),
                Arguments.of(Orientation.SOUTH, new Position(2, 1))
        );
    }
}
