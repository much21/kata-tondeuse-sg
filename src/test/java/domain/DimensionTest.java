package domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DimensionTest {

    @Test
    public void shouldReturnTrue() {
        Position position = new Position(1, 1);
        Dimension dimensionTest = new Dimension(3, 3);

        boolean result = Dimension.isInside(position, dimensionTest);

        assertTrue(result);
    }

    @Test
    public void shouldReturnFalse() {
        Position position = new Position(4, 1);
        Dimension dimensionTest = new Dimension(3, 3);

        boolean result = Dimension.isInside(position, dimensionTest);

        assertFalse(result);
    }
}
