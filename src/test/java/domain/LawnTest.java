package domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LawnTest {

    @Mock
    Mower mower;

    @Mock
    Mower secondMower;

    @Test
    public void shouldMowableForward() {
        Lawn lawn = new Lawn(new Dimension(6,6), List.of(mower));

        when(mower.getPosition())
                .thenReturn(new Position(3,3))
                .thenReturn(new Position(3,3))
                .thenReturn(new Position(3,3))
                .thenReturn(new Position(4, 3));
        when(mower.getInstructions()).thenReturn(List.of(Instruction.FORWARD));
        when(mower.getOrientation()).thenReturn(Orientation.NORTH);

        List<MowerMovementResult> expected = List.of(new MowerMovementResult(new Position(4,3), Orientation.NORTH));
        List<MowerMovementResult> result = lawn.mow();

        verify(mower).goForward();
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotMowableForwardWhenDestinationIsOutside() {
        Lawn lawn = new Lawn(new Dimension(4,4), List.of(mower));

        when(mower.getPosition()).thenReturn(new Position(2,4));
        when(mower.getInstructions()).thenReturn(List.of(Instruction.FORWARD));
        when(mower.getOrientation()).thenReturn(Orientation.NORTH);

        List<MowerMovementResult> expected = List.of(new MowerMovementResult(new Position(2,4), Orientation.NORTH));
        List<MowerMovementResult> result = lawn.mow();

        verifyNoMoreInteractions(mower);
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotMowableForwardWhenDestinationIsOccupied() {
        Lawn lawn = new Lawn(new Dimension(4,4), List.of(mower, secondMower));

        when(mower.getPosition()).thenReturn(new Position(2,2));
        when(mower.getInstructions()).thenReturn(List.of(Instruction.FORWARD));
        when(mower.getOrientation()).thenReturn(Orientation.SOUTH);

        when(secondMower.getPosition()).thenReturn(new Position(2,1));
        when(secondMower.getInstructions()).thenReturn(List.of());
        when(secondMower.getOrientation()).thenReturn(Orientation.WEST);

        List<MowerMovementResult> expected = List.of(
                new MowerMovementResult(new Position(2,2), Orientation.SOUTH),
                new MowerMovementResult(new Position(2,1), Orientation.WEST)
        );
        List<MowerMovementResult> result = lawn.mow();

        verifyNoMoreInteractions(mower,secondMower);
        assertEquals(expected, result);
    }


    @Test
    public void shouldMowableLeft() {
        Lawn lawn = new Lawn(new Dimension(6,6), List.of(mower));

        when(mower.getPosition()).thenReturn(new Position(3,3));
        when(mower.getInstructions()).thenReturn(List.of(Instruction.LEFT));
        when(mower.getOrientation()).thenReturn(Orientation.WEST);

        List<MowerMovementResult> expected = List.of(new MowerMovementResult(new Position(3,3), Orientation.WEST));
        List<MowerMovementResult> result = lawn.mow();

        verify(mower).goLeft();
        assertEquals(expected, result);
    }

    @Test
    public void shouldMowableRight() {
        Lawn lawn = new Lawn(new Dimension(6,6), List.of(mower));

        when(mower.getPosition()).thenReturn(new Position(3,3));
        when(mower.getInstructions()).thenReturn(List.of(Instruction.RIGHT));
        when(mower.getOrientation()).thenReturn(Orientation.EAST);

        List<MowerMovementResult> expected = List.of(new MowerMovementResult(new Position(3,3), Orientation.EAST));
        List<MowerMovementResult> result = lawn.mow();

        verify(mower).goRight();
        assertEquals(expected, result);
    }
}

